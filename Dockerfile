FROM golang:1.7

RUN apt-get update && apt-get install git
RUN go get bitbucket.org/engineerbetter/cfcd-parser/cmd/cfcd-item-parser && \
    go get bitbucket.org/engineerbetter/cfcd-parser/cmd/cfcd-itemfact-sorter && \
    go get bitbucket.org/engineerbetter/cfcd-parser/cmd/cfcd-item-validator
